package TuxTARDIS;

import TuxTARDIS.Mechanics.Mechanics;
import TuxTARDIS.Mechanics.To;
import TuxTARDIS.Navigation.Follower;
import TuxTARDIS.Navigation.Map;
import TuxTARDIS.Navigation.Path;
import TuxTARDIS.Utils.Effects;
import TuxTARDIS.Utils.MapSelector;

import lejos.hardware.Button;
import lejos.hardware.Sound;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Random;

/**
 * Třída hlavního programu
 */
public class Main {
	/**
	 * Zkratka pro informační výpisy
	 */
	private static final String main = "INIT";

	/**
	 * Hlavní funkce programu, entry-point
	 *
	 * @param nic Ignorované argumenty
	 */
	public static void main(String[] nic) {
		Button.LEDPattern(5); // bliká červená
		Const.load();
		Button.LEDPattern(3); // svítí zelená
		Effects.twobeep();
		MapSelector select = new MapSelector("Zadejte cislo:");
		int mn = select.getNumber(1, Const.MAPS, 1); // zeptá se na číslo mapy
		Button.LEDPattern(6); // bliká oranžová
		Debug.info(main, "textak");
		String mapStr = load_txt(Const.MAP_DIR+"/"+mn+".map"); // načte mapu
		Debug.info(main, "robot");
		Mechanics robot = new Mechanics(); // inicializuje fyz. rozhranní robota
		robot.setPercentSpeed(Const.SPEED); // nastaví rychlost
		Debug.info(main, "mapa");
		Map map = new Map(mapStr); // inicializuje mapu
		Debug.info(main, "cesta");
		Path path = new Path(mapStr); // inicializuje cestu
		Debug.info(main, "cache");
		Scheduler sched = new Scheduler(robot, map); // inicializuje cache
		Debug.info(main, "follower");
		Follower navigation = new Follower(map, sched); // inicializuje procházeč

		Button.LEDPattern(1); // svítí zelená
		System.out.println("* ENTER -> RUN *");
		Effects.twobeep();
		Button.ENTER.waitForPress();

		Button.LEDPattern(4); // bliká zelená
		Debug.info(main, "vypisovac");
		map.initPrint(); // inicializuje vypisovač
		map.print.clear();
		Debug.info(main, "OK jedem!");
		navigation.Follow(path); // spustí samotnou cestu

		// DANGER!!! DO NOT REMOVE!!! OTHERWISE I'LL KILL YOU!!!
		sched.dispose(); // SAFETY HACK
		// END OF DANGER
		Button.LEDPattern(1); // svítí oranžová
		Effects.play_tune(); // zahraje šílenou melodii :D
		while(Button.ESCAPE.isUp()) {
			robot.travel(randInt(1,6));
			int otoceni = randInt(1,4);
			switch(otoceni){
				case 0:
					robot.turn(90, To.Left);
					break;
				case 1:
					robot.turn(90,To.Right);
					break;
				case 2:
					robot.travel(-Const.TILE_LENGTH*randInt(1,3));
					robot.turn(90,To.Right);
					break;
				case 3:
					robot.travel(-Const.TILE_LENGTH*randInt(1,3));
					robot.turn(90,To.Left);
					break;
			}
		}
	}

	/**
	 * Přečte texťák
	 *
	 * @param name Jméno souboru
	 * @return Text souboru
	 */
	public static String load_txt(String name) {
		StringBuilder sb = new StringBuilder(512); // buffer souboru
		try { // proti vyjímce
			FileInputStream stream = new FileInputStream(name); // otevře soubor
			Reader r = new InputStreamReader(stream, "UTF-8"); // otevře čteč souboru
			int c; // přečte soubor
			while ((c = r.read()) != -1) {
				sb.append((char) c);
			}
			stream.close(); // zavře soubor
		} catch (IOException e) {
			Sound.beepSequenceUp();
			Sound.beepSequenceUp();
			Sound.beepSequenceUp();
			throw new RuntimeException(e);
		}
		return sb.toString(); // vrátí buffer
	}
	public static int randInt(int min, int max) {

		// NOTE: Usually this should be a field rather than a method
		// variable so that it is not re-seeded every call.
		Random rand = new Random();

		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		return rand.nextInt((max - min) + 1) + min;
	}
}