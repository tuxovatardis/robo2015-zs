package TuxTARDIS;

import TuxTARDIS.Mechanics.Mechanics;
import TuxTARDIS.Mechanics.To;
import TuxTARDIS.Navigation.Map;
import TuxTARDIS.Navigation.MapOrient;
import TuxTARDIS.Navigation.Pos;
import TuxTARDIS.Navigation.TileType;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Cache
 */
public class Scheduler {
	/**
	 * Fyz. rozhranní robota
	 */
	private final Mechanics mech;
	/**
	 * Mapa bludiště
	 */
	private final Map map;
	/**
	 * Fronta příkazů
	 */
	private final Queue<CMD> commands = new LinkedList<>();
	/**
	 * Čítač virtuální odchylky
	 */
	private int error;
	/**
	 * Čítač přeskočení
	 */
	private int fwd_align_skip = 0;


	/**
	 * Konstruktor
	 *
	 * @param robot Mechanics
	 * @param map   Mapa
	 */
	public Scheduler(Mechanics robot, Map map) {
		this.mech = robot;
		this.map = map;
	}

	/**
	 * Výčet příkazů
	 */
	public enum CMD {
		Forward, Backward, Left, Right
	}

	/**
	 * Přidá příkaz pro jízdu dopředu
	 */
	public void forward() {
		commands.add(CMD.Forward); // přidá do fronty
		error++; // přidá na čítač virtuální odchylky
		map.travel(1, false); // popojede v mapě
		process_hook(); // zavolá hook
	}

	/**
	 * Přidá příkaz pro jízdu do
	 */
	public void backward() {
		commands.add(CMD.Backward); // přidá do fronty
		error++; // přidá na čítač virtuální odchylky
		map.travel(1, true); // popojede v mapě
		process_hook(); // zavolá hook
	}

	/**
	 * Přidá příkaz pro jízdu do
	 */
	public void left() {
		commands.add(CMD.Left); // přidá do fronty
		error += 2; // přidá na čítač virtuální odchylky (váha 2)
		map.turn(To.Left, 1); // pootočí se v mapě
		process_hook(); // zavolá hook -> vykonání
	}

	/**
	 * Přidá příkaz pro jízdu do
	 */
	public void right() {
		commands.add(CMD.Right); // přidá do fronty
		error += 2; // přidá na čítač virtuální odchylky (váha 2)
		map.turn(To.Right, 1); // pootočí se v mapě
		process_hook(); // zavolá hook -> vykonání
	}


	/**
	 * Funkce pro vykonávání a zarovnávání robota
	 */
	private void process_hook() {
		// TEST MOŽNOSTI ZAROVNÁNÍ
		boolean force = false; // zarovnat?
		int difference = 0; // o kolik otočit?

		if (Const.ENABLE_ALIGN_COUNTER && error > Const.ERROR_LIMIT) { // pokud je povoleno a pokud přetekl limit
			Pos[] nearest = Pos.adjacent(map.curPos, map.curOrient); // zjisti sousední pozice
			for (Pos field : nearest) { // těmi projdi
				if (map.get(field) == TileType.Wall) { // pokud mezi nimi je zeď
					int dir = Pos.directionOf(map.curPos, field); // zjisti směr odsud
					difference = MapOrient.difference(map.curOrient, dir); // zjisti odchylku směru od současného
					force = true; // nastav zarovnání
					break; //vyskoč
				}
			}
		}

		// PŘÍPADNÉ VYPRÁZDNĚNÍ CACHE = VYKONÁNÍ PRÁCE
		// jeď, pokud 1) je zakázané cache, nebo 2) je možnost zarovnání robota, nebo 3) ve frontě není jen dopředu/dozadu
		boolean homogenous = check_homogenity() && !force && Const.ENABLE_CACHING;
		if (!homogenous) {
			process_queue(); // vyčisti cache & jeď robotem
		}

		// PŘÍPADNÉ ZAROVNÁNÍ
		if (Const.ENABLE_ALIGN_COUNTER && force) { // pokud je povoleno a pokud se dá zarovnat
			if (difference != 0)  // otoč se (pokud je to potřeba)
				mech.turn(Math.abs(difference * 90), difference > 0 ? To.Right : To.Left);

			mech.align();

			if (difference != 0)  // otoč se zpět (pokud je to potřeba)
				mech.turn(Math.abs(difference * 90), difference > 0 ? To.Left : To.Right);
			error = 0; // resetuj počítadlo
		}
	}


	/**
	 * Zjistí, jestli jsou ve frontě jen CMD.Forward a CMD.Backward
	 *
	 * @return Nález
	 */
	private boolean check_homogenity() {
		if (commands.size() == 0) // pokud nic nemáme
			return true; // ano OK
		Queue<CMD> temp = new LinkedList<>(commands); // zkopírujeme frontu
		while (temp.size() > 0) { // dokud jí nevyprázdníme
			CMD test = temp.poll(); // vezmi si příkaz
			if (test != CMD.Backward && test != CMD.Forward) // pokud to není dopředu/dozadu
				return false; // ne, fronta není homogenní
		}
		return true; // ano, fronta je homogenní
	}

	/**
	 * Vykoná cache
	 */
	private void process_queue() {
		while (commands.size() > 0) { // dokud nevyčerpáme příkazy
			CMD task = commands.peek(); // nakoukni na příkaz

			int count = count_elements(); // spočítej počet příkazů
			if (task == CMD.Forward) { // pokud máme jet dopředu
				boolean align = false;
				if (Const.ENABLE_FWD_ALIGN) { // pokud můžeme dorážet na zeď
					if (Const.FWD_ALIGN_SKIP == -1 || fwd_align_skip < Const.FWD_ALIGN_SKIP) { // pokud neskipujeme
						align = test_fwd_align(); // zkus jestli lze narazit
						fwd_align_skip++; // zvyš čítač
					} else
						fwd_align_skip = 0; // resetuj čítač
				}
				if(align)
					mech.align();
				else
					mech.travel(count * Const.TILE_LENGTH); // jeď
			} else if (task == CMD.Backward) { // pokud máme jet dozadu
				mech.travel(-count * Const.TILE_LENGTH); // jeď
			} else if (task == CMD.Left) { // pokud máme zatočit doleva
				mech.turn(90 * count, To.Left); // zatoč
			} else if (task == CMD.Right) { // pokud máme zatočit doprava
				mech.turn(90 * count, To.Right); // zatoč
			}
		}
	}

	/**
	 * Zjistli, jestli lze narazit na zeď před robotem, pokud ano, srovnej robota
	 */
	private boolean test_fwd_align() {
		CMD[] prikazy = commands.toArray(new CMD[commands.size()]); // zkopírujeme frontu
		int curOrient = map.curOrient; // zkopírujeme současný směr
		Pos curPos = map.curPos; // zkopírujeme současnou pozici
		for (int i = prikazy.length - 1; i >= 0; i--) { // postupně se skrz frontu prokousej na reálnou současnou pozici
			switch (prikazy[i]) {
				case Forward: // pokud bychom jeli dopředu
					curPos = Pos.modify(curPos, curOrient, -1); // dozadu
					break;
				case Backward: // pokud bychom jeli dozadu
					curPos = Pos.modify(curPos, curOrient, 1);// dopředu
					break;
				case Left: // pokud bychom zatáčeli doleva
					curOrient = MapOrient.edit(curOrient, 1);// doprava
					break;
				case Right: // pokud bychom zatáčeli doprava
					curOrient = MapOrient.edit(curOrient, -1);// doleva
					break;
			}
		}
		return map.get(Pos.modify(curPos, curOrient, 1)) == TileType.Wall;
	}

	/**
	 * Spočítá počet příkazů, které také odebere.
	 *
	 * @return Počet stejných příkazů
	 */
	private int count_elements() {
		CMD first = commands.poll(); // získá první příkaz
		if (first == null) // pokud žádný není
			return 0; // vrať 0

		int count = 1; // počet je min. jeden - už jsme jeden vzali
		while (commands.size() > 0) { // dokud nedojdou příkazy
			if (commands.peek() != first) // pokud se další neshoduje s prvním
				break; // stop
			count++; // zvyš čítač
			commands.poll(); // seber ten, na který jsme nakoukli přes peek()
		}
		return count; // vrať počet
	}

	/**
	 * Dokončí práci z fronty
	 */
	public void dispose() {
		while (commands.size() > 0) // dokud máme příkazy
			process_queue(); // vykonávej
	}
}