package TuxTARDIS.Mechanics;

/**
 * Směr otáčení
 */
public enum To {
	Right, Left
}
