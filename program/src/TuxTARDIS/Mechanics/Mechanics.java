package TuxTARDIS.Mechanics;
import TuxTARDIS.Const;
import TuxTARDIS.Debug;
import TuxTARDIS.Utils.Common;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

/**
 * Fyzické rozhranní robota
 */
public class Mechanics {
	/**
	 * Poměr otočení robota a kol
	 */
	private final double turn_rate;
	/**
	 * Obvod kol
	 */
	private final double forward_rate;
	/**
	 * Aktuální úhlová rychlost
	 */
	private int speed;
	/**
	 * Pravý motor
	 */
	private final RegulatedMotor right;
	/**
	 * Levý motor
	 */
	private final RegulatedMotor left;
	/**
	 * Gyroskop
	 */
	private final EV3GyroSensor gyro;
	/**
	 * Gyroskop
	 */
	private final EV3TouchSensor touch;

	/**
	 * Výchozí konstruktor
	 */
	public Mechanics() {
		Debug.info("MECH", "init");
		turn_rate = (Const.TRACKWIDTH / Const.DIAMETER); // výpočty konstant
		forward_rate = Const.DIAMETER * Math.PI;

		right = new EV3LargeRegulatedMotor(Const.RIGHT_PORT); // inicializace motorů
		left = new EV3LargeRegulatedMotor(Const.LEFT_PORT);
		touch = new EV3TouchSensor(Const.TOUCH_PORT);

		left.synchronizeWith(new RegulatedMotor[]{right}); // synchronizace
		left.startSynchronization();
		right.setAcceleration(Const.ACCEL); // nastavit zrychlení motorů
		left.setAcceleration(Const.ACCEL);
		right.setStallThreshold(Const.STALL_LAG, Const.STALL_TIME); // nastavit limit zaseknutí
		left.setStallThreshold(Const.STALL_LAG, Const.STALL_TIME);
		left.endSynchronization();

		Debug.info("MECH", "gyro init");
		gyro = new EV3GyroSensor(Const.GYRO_PORT); // inicializace gyroskopu
		gyro.reset(); // reset & kalibrace gyroskopu
		Debug.info("MECH", "OK");
	}

	/**
	 * Spočítá otočení kol z otočení robota
	 *
	 * @param degree Otočení robota ve stupních
	 * @return Otočení kol (strany +- nebo -+)
	 */
	private int turn_size(int degree) {
		return (int) Math.round(turn_rate * degree);
	}

	/**
	 * Spočítá otočení kol ze vzdálenosti k ujetí
	 *
	 * @param distance Vzdálenost k ujetí
	 * @return Otočení kol (strany ++ nebo --)
	 */
	private int forward_size(float distance) {
		return (int) Math.round(distance / forward_rate * 360);
	}


	/**
	 * Otočí robota o daný počet stupňů daným směrem
	 *
	 * @param degrees   Otočení robota ve stupních
	 * @param direction Směr otáčení
	 */
	public void turn(int degrees, To direction) {
		turn_P(degrees, direction);
	}

	/**
	 * Otočí robota o daný počet stupňů daným směrem s korekcí pomocí P regulátoru
	 * @param degrees Otočení robota ve stupních
	 * @param direction Směr otáčení
	 */
	public void turn_P(int degrees, To direction){
		gyro.reset(); // reset gyroskopu
		SampleProvider data = gyro.getAngleMode(); // získání vzorkovače
		Common.getFirst(data); // HACK pro rozběhnutí vzorkovače

		int kola = turn_size(degrees); // získání potřebného otočení kol
		//putSpeed(speed / 2); // poloviční rychlost obou kol
		int sign = direction == To.Right ? 1 : -1; // nastavení opravného znaménka

		left.startSynchronization(); // synchronizované točení
		left.rotate(sign * kola, true);
		right.rotate(sign * -kola, true);
		left.endSynchronization();
		waitComplete(); // počká na konec

		while (true) { // "donekonečna"
			float angle = Common.getFirst(data); // získej úhel otočení
			float odchylka = degrees + sign * angle; // spočti odchylku
			if (Math.abs(odchylka) < Const.MAXERROR) // pokud je odchylka malá
				break; // přeruš nekonečno

			int fix = turn_size((int) (Const.TURN_KP * odchylka)); // spočti opravu
			left.startSynchronization(); // a dotoč
			left.rotate(sign * fix, true);
			right.rotate(sign * -fix, true);
			left.endSynchronization();
			waitComplete();
		}
		//putSpeed(speed); // nastav původní rychlost
	}
	/**
	 * Ujede danou vzdálenost
	 *
	 * @param distance Vzdálenost k ujetí
	 */
	public void travel(int distance) {
		int kola = forward_size(distance); // spočte otočení
		if(Const.ENABLE_TRAVEL_GYRO) {
			gyro.reset(); // reset gyroskopu
			SampleProvider data = gyro.getAngleMode(); // získání vzorkovače
			Common.getFirst(data); // hack

			left.resetTachoCount(); // reset tachometru
			right.resetTachoCount();
			if (distance > 0) { // start otáčení
				left.startSynchronization();
				left.forward();
				right.forward();
				left.endSynchronization();
			} else {
				left.startSynchronization();
				left.backward();
				right.backward();
				left.endSynchronization();
			}

			monitorWait(data, kola); // konec otáčení, korekce
		}else{
			left.startSynchronization(); // otáčení
			left.rotate(kola);
			right.rotate(kola);
			left.endSynchronization();
			waitComplete(); // počkání do konce
		}

	}

	/**
	 * Pomocná proměnná pro monitorWait() se zapnutým Const.ENABLE_TRAVEL_GYRO
	 */
	private static int fix=0;
	/**
	 * Čeká, dokud se motory nezastaví nebo nezaseknou
	 */
	private void monitorWait(final SampleProvider data, final int kola) {
		if(Const.ENABLE_TRAVEL_GYRO) { // pokud je korekce (občas destrukce) povolená
			Thread thr = new Thread() { // gyro thread
				@Override
				public void run() {
					while (!this.isInterrupted()) { // dokud není přerušen
						float error = Common.getFirst(data); // získej gyroskop
						fix = turn_size(Math.round(error * Const.FWD_KP)); // spočti opravu
					}
				}
			};
			thr.start(); // start threadu
			while (true) { // dokud se motory pohybují
				left.startSynchronization(); // získej zbytek
				int rest_L = kola - left.getTachoCount();
				int rest_R = kola - right.getTachoCount();
				left.endSynchronization();

				boolean left_request = kola > 0 ? rest_L <= 0 : rest_L >= 0; // požadujeme stop?
				boolean right_request = kola > 0 ? rest_R <= 0 : rest_R >= 0;

				if (left_request || right_request) { // pokud požadujeme stop
					stop(); // zastav motory
					thr.interrupt(); // zastav thread
					fix = 0; // resetuj fix
					putSpeed(speed); // nastav původní rychlost
					left.startSynchronization(); // oprav přejetou/nedojetou pozici
					left.rotate(rest_L, true);
					right.rotate(rest_R, true);
					left.endSynchronization();
					waitComplete(); // čekej
					break; // exit
				}
				left.startSynchronization(); // změní rychlost o korekci, ta se počítá v threadu thr
				left.setSpeed(speed + fix);
				right.setSpeed(speed - fix);
				left.endSynchronization();

				if (kola > 0) { // podle toho, jestli jedeme dozadu nebo dopředu
					left.startSynchronization(); // updatuj rychlsot dopředu
					left.forward();
					right.forward();
					left.endSynchronization();
				} else {
					left.startSynchronization(); // updatuj rychlsot dozadu
					left.backward();
					right.backward();
					left.endSynchronization();
				}
			}
		}
		else // když je zakázána mrvící korekce
			waitComplete(); // čekej
	}

	/**
	 * Čeká, dokud se motory nezastaví
	 */
	private void waitComplete() {
		while (left.isMoving() || right.isMoving()) {
			left.waitComplete();
			right.waitComplete();
		}
	}


	/**
	 * Nastaví rychlost robota
	 *
	 * @param speed Procenta maximální rychlosti
	 */
	public void setPercentSpeed(float speed) {
		this.speed = (int) (Math.min(left.getMaxSpeed(), right.getMaxSpeed()) * speed / 100); // přepočte procenta na °/s
		putSpeed(this.speed); // nastaví
	}

	/**
	 * Nastaví rychlost motorů
	 *
	 * @param speed Úhlová rychlost v °/s
	 */
	private void putSpeed(int speed) {
		left.startSynchronization();
		right.setSpeed(speed);
		left.setSpeed(speed);
		left.endSynchronization();
	}

	/**
	 * Zapne robota dopředu
	 */
	public void forward() {
		left.startSynchronization();
		left.forward();
		right.forward();
		left.endSynchronization();
	}

	/**
	 * Zastaví robota
	 */
	public void stop() {
		left.startSynchronization();
		left.stop(true);
		right.stop(true);
		left.endSynchronization();
	}

	/**
	 * Zarovná robota
	 */
	public void align() {
		if(Const.ENABLE_FORWARD_GYRO) // pokud máme korekci
			gyro.reset(); // resetuj gyroskop
		SampleProvider data = gyro.getAngleMode(); // vzorkovač gyroskopu
		SampleProvider button = touch.getTouchMode(); // vzorkovač tlačítka
		if(Const.ENABLE_FORWARD_GYRO) // pokud máme korekci
			Common.getFirst(data); // hack
		forward(); // jeď dopředu
		while (true) { // donekonečna
			if (Common.getFirst(button) == 1) // pokud je tlačítko zmáčknuté
				break; // konec
			if(Const.ENABLE_FORWARD_GYRO) { // pokud máme korekci
				float angle = Common.getFirst(data); // zjisti úhel
				int fix = turn_size(Math.round(angle * Const.FWD_KP)); // spočti opravu

				int fixL = speed + fix; // spočti změny rychlosti
				int fixR = speed - fix;

				left.startSynchronization(); // nastaví rychlost
				left.setSpeed(fixL > 0 ? fixL : 0);
				right.setSpeed(fixR > 0 ? fixR : 0);
				left.endSynchronization();

				left.startSynchronization(); // updatne rychlsot
				left.forward();
				right.forward();
				left.endSynchronization();
			}
			else // pokud nemáme korekci
				Thread.yield(); // už nemáme nic k práci
		}
		putSpeed(speed); // nastav původní rychlost
		Delay.msDelay(Const.TRAVELBACK_DELAY); // čekej
		travel(Const.TRAVELBACK); // popojeď zpět + zastav
	}
}