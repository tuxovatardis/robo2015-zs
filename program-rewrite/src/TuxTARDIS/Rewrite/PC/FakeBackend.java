package TuxTARDIS.Rewrite.PC;

import TuxTARDIS.Rewrite.Mechanics.HardwareRouter;
import TuxTARDIS.Rewrite.Navigation.Map;
import TuxTARDIS.Rewrite.Utils.Common;

/**
 *
 */
public class FakeBackend extends HardwareRouter  {
	private final Map map;
	public FakeBackend(Map map){
		this.map = map;
	}
	@Override
	public void travel(double distance) {
		System.out.println("TRAVEL="+distance);
		waitHook();
	}

	@Override
	public void stop() {
		System.out.println("STOP");
		waitHook();
	}

	@Override
	public void rotate(double angle) {
		System.out.println("ROTATE="+angle);
		waitHook();
	}

	@Override
	public void alignBack() {
		System.out.println("ALIGN_BACK");
		waitHook();
	}

	@Override
	public void alignFront() {
		System.out.println("ALIGN_FRONT");
		waitHook();
	}

	@Override
	public void forward() {
		System.out.println("FWD");
		waitHook();
	}

	@Override
	public void backward() {
		System.out.println("BWD");
		waitHook();
	}

	@Override
	public void setTravelSpeed(double speed) {
		System.out.println("TRAVEL_SPD="+speed);
	}

	@Override
	public void setRotateSpeed(double speed) {
		System.out.println("ROTATE_SPD="+speed);
	}

	@Override
	public void setAcceleration(int accel) {
		System.out.println("ACCEL="+accel);
	}

	@Override
	public float getSonic() {
		return 0;
	}

	@Override
	public float getSonic(int rotateQ, boolean goBack) {
		rotate(rotateQ * 90);
		float value = getSonic();
		if(goBack)
			rotate(-rotateQ*90);
		return value;
	}

	@Override
	public float getGyro() {
		return 0;
	}

	@Override
	public boolean getFrontTouch() {
		return Common.randInt(0,1)==1;
	}

	@Override
	public boolean getBackTouch() {
		return Common.randInt(0,1)==0;
	}
	@Override
	public void resetGyro() {
		System.out.println("GYRO_RESET");
	}
	public void waitForPress(){
		System.out.println("Press enter to continue.");
		try {
			System.in.read();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public boolean getContinue(){
		return Main_PC.getInt("Do you want to continue to next round? (0/1): ")==1;
	}
	private void waitHook(){
		try {
			Thread.sleep(10);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
