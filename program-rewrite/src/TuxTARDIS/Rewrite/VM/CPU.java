package TuxTARDIS.Rewrite.VM;

import TuxTARDIS.Rewrite.Const;
import TuxTARDIS.Rewrite.Mechanics.HardwareRouter;
import TuxTARDIS.Rewrite.Navigation.*;
import TuxTARDIS.Rewrite.Utils.CMD;
import TuxTARDIS.Rewrite.Utils.Common;
import TuxTARDIS.Rewrite.Utils.FloatRange;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Central Processor Unit - executes generated instructions
 */
public class CPU {
	private final HardwareRouter hardware;
	private int expectedGyro = 0;

	public CPU(HardwareRouter hardware) {
		this.hardware = hardware;
	}

	public static BlockingQueue<Instruction> streamize(Map map, Path path) {
		BlockingQueue<Instruction> pack_measure = new LinkedBlockingQueue<>();
		BlockingQueue<Instruction> measure_cpu = new LinkedBlockingQueue<>();

		Packer pack = new Packer(new Map(map),path,pack_measure);
		MeasurePredictor predictor = new MeasurePredictor(new Map(map),pack_measure,measure_cpu);

		Thread packThr = new Thread(pack);
		Thread predictThr = new Thread(predictor);
		packThr.setDaemon(true);
		predictThr.setDaemon(true);

		packThr.run();
		predictThr.run();
		return measure_cpu;
	}

	public void runStream(BlockingQueue<Instruction> exe, Map map) {
		while (true) {
			Instruction cmd = Common.getNext(exe);
			if(cmd instanceof ExitInstruction)
				break;
			else if (cmd instanceof MoveInstruction) {
				MoveInstruction move = (MoveInstruction) cmd;
				switch (move.getType()) {
					case MoveInstruction.FORWARD:
						hardware.travel(Const.TILE_LENGTH * move.getCount());
						map.travel(move.getCount(), false);
						break;
					case MoveInstruction.BACKWARD:
						hardware.travel(-Const.TILE_LENGTH * move.getCount());
						map.travel(move.getCount(), true);
						break;
					case MoveInstruction.FRONT_ALIGN:
						hardware.alignFront();
						map.travel(map.beforeWall(), false);
						break;
					case MoveInstruction.BACK_ALIGN:
						map.travel(map.beforeWall(MapOrient.edit(map.curOrient, 2), map.curPos), true);
						break;
					case MoveInstruction.TURN_LEFT:
						hardware.rotate(90 * move.getCount());
						map.turn(CMD.ToLeft, move.getCount());
						expectedGyro += 90;
						break;
					case MoveInstruction.TURN_RIGHT:
						hardware.rotate(-90 * move.getCount());
						map.turn(CMD.ToRight, move.getCount());
						expectedGyro -= 90;
						break;
				}
				fix(move);

			} else if (cmd instanceof ConfInstruction) {
				switch (cmd.getType()) {
					case ConfInstruction.SPEED:
						hardware.setTravelSpeed(((ConfInstruction) cmd).getValue());
						hardware.setRotateSpeed(((ConfInstruction) cmd).getValue() * 2 / 3);
						break;
					case ConfInstruction.ACCEL:
						hardware.setAcceleration(((ConfInstruction) cmd).getValue());
						break;
				}
			}
		}
	}

	private void fix(MoveInstruction move) {
		// rotate to correct direction
		hardware.rotate(expectedGyro - hardware.getGyro());
		// verify values
		int newDir = move.getEndOrient();
		FloatRange expectedSonic = move.getExpectedSonic()[newDir];
		float sonic = hardware.getSonic();
		if (expectedSonic.fits(sonic))
			return;
		FloatRange[] expectedSonicDir = move.getExpectedSonic();
		FloatRange expectedSonicBack = expectedSonicDir[MapOrient.edit(move.getEndOrient(), 2)];
		// oops, bug, let's fix that FIXME tohleto musím buď odpálit nebo spravit
		if (expectedSonic.getCorrect() == Float.POSITIVE_INFINITY) {
			if (sonic == Float.POSITIVE_INFINITY) {
				float backSonicExpect = expectedSonicBack.getCorrect();
				float backSonicReal = hardware.getSonic(2, true);
				float distance = backSonicExpect - backSonicReal;
				hardware.travel(distance);
			} else {
				hardware.backward();
				while (hardware.getSonic() != Float.POSITIVE_INFINITY)
					Thread.yield();
				hardware.stop();
				float backSonicExpect = expectedSonicBack.getCorrect();
				float backSonicReal = hardware.getSonic(2, true);
				float distance = backSonicExpect - backSonicReal;
				hardware.travel(distance);
			}
		} else {
			if (sonic == Float.POSITIVE_INFINITY) {
				// travel forward, until wall is in reach of sonic
				hardware.forward();
				while (!expectedSonic.fits(hardware.getSonic()))
					Thread.yield();
				hardware.stop();
				// align normally
				float distance = expectedSonic.getCorrect()
						- hardware.getSonic();
				hardware.travel(distance);
			} else {
				// align normally
				float distance = expectedSonic.getCorrect()
						- hardware.getSonic();
				hardware.travel(distance);
			}
		}
	}
}