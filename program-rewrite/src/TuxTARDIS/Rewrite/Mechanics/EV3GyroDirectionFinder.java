package TuxTARDIS.Rewrite.Mechanics;

import TuxTARDIS.Rewrite.Utils.Common;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.robotics.DirectionFinder;

/**
 * Compatibility layer for old sensor framework
 */
class EV3GyroDirectionFinder implements DirectionFinder {
	private final EV3GyroSensor sensor;
	public EV3GyroDirectionFinder(EV3GyroSensor gyro){
		this.sensor=gyro;
	}
	@Override
	public float getDegreesCartesian() {
		return Common.getFirst(sensor.getAngleMode());
	}

	@Override
	public void resetCartesianZero() {
		sensor.reset();
	}

	@Override
	public void startCalibration() {
		sensor.reset();
	}

	@Override
	public void stopCalibration() {

	}
}
