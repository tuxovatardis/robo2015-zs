package TuxTARDIS.Rewrite.Mechanics;

import TuxTARDIS.Rewrite.Const;
import TuxTARDIS.Rewrite.Utils.Common;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.robotics.SampleProvider;
import lejos.robotics.navigation.TuxCompassPilot;
import lejos.utility.Delay;

/**
 * Proxy for {@link TuxCompassPilot} (clone of {@link lejos.robotics.navigation.CompassPilot})
 */
public class TuxCompassPilotProxy implements TuxPilot {
	private final TuxCompassPilot pilot;
	private final EV3TouchSensor rear;
	private final EV3TouchSensor front;
	private final double travelback;
	public TuxCompassPilotProxy(PilotProperties props){
		this.pilot = new TuxCompassPilot(new EV3GyroDirectionFinder(props.getGyro()),
				props.getWheelDiameter(),props.getTrackWidth(),props.getLeftMotor(),props.getRightMotor(),false);
		this.rear = props.getBackTouch();
		this.front = props.getFrontTouch();
		this.travelback = props.getTravelback();
	}
	@Override
	public void travel(double distance) {
		pilot.travel(distance,true);
		SampleProvider touch = distance >= 0 ? front.getTouchMode() : rear.getTouchMode();
		while(Common.getFirst(touch)==0 &&  pilot.isMoving())
			Thread.yield();
		pilot.stop();
	}

	@Override
	public void stop() {
		pilot.stop();
	}

	@Override
	public void rotate(double angle) {
		pilot.rotate((float)angle);
	}

	@Override
	public void alignBack() {
		pilot.backward();
		while(Common.getFirst(rear.getTouchMode())==0) Thread.yield();
		Delay.msDelay(Const.TRAVELBACK_DELAY); // čekej
		pilot.travel(travelback);
	}

	@Override
	public void alignFront() {
		pilot.forward();
		while(Common.getFirst(front.getTouchMode())==0) Thread.yield();
		Delay.msDelay(Const.TRAVELBACK_DELAY); // čekej
		pilot.travel(-travelback);
	}

	@Override
	public void forward() {
		pilot.forward();
	}

	@Override
	public void backward() {
		pilot.backward();
	}

	@Override
	public void setTravelSpeed(double speed) {
		pilot.setTravelSpeed(speed);
	}

	@Override
	public void setRotateSpeed(double speed) {
		pilot.setRotateSpeed(speed);
	}

	@Override
	public void setAcceleration(int accel) {
		pilot.setAcceleration(accel);
	}
}
