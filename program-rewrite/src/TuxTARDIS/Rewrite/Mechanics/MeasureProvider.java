package TuxTARDIS.Rewrite.Mechanics;

/**
 * Hardware for measuring physical quantities
 */
public interface MeasureProvider {
	float getSonic();
	float getSonic(int rotateQ, boolean goBack);
	float getGyro();
	boolean getFrontTouch();
	boolean getBackTouch();
	void resetGyro();
}
