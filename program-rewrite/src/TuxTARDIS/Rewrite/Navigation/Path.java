package TuxTARDIS.Rewrite.Navigation;

import TuxTARDIS.Rewrite.Utils.Common;
import java.util.*;

/**
 * Trasa/Cesta (Queue)
 */
public class Path {
	private static final int WALL = -1;
	/**
	 * Interní reprezentace trasy, fronta souřadnic
	 */
	public Queue<Pos> path;

	/**
	 * Konstruktor
	 *
	 * @param map Obsah cesty
	 */
	public Path(String map) {
		this.load(map);
	}


	/**
	 * Konstruktor
	 *
	 * @param path Obsah cesty
	 */
	public Path(Queue<Pos> path) {
		this.path = path;
	}
	/**
	 * Načte cestu z textového obsahu
	 *
	 * @param data Textový obsah cesty
	 */
	private void load(String data) {
		// PARSER
		Integer[][][] map = new Integer[Map.sizeX][Map.sizeY][]; // mapa seznamů pořadí
		data = data.replaceAll("\\s", ""); // odstraní mezery, nové řádky
		data = data.replaceAll("\\n", "");
		String fields[] = data.split(","); // rozdělí text na jednotlivá pole
		int index = 0; // iterace
		for (int y = 0; y < Map.sizeY; y++) { // procházej po sloupcích
			for (int x = 0; x < Map.sizeX; x++) { // v každém sloupci procházej po řádcích
				if (!fields[index].equals("X")) { // pokud zde není zeď
					List<Integer> elements = new ArrayList<>(); // seznam pořadí
					for (String element : fields[index].split("\\+")) // rozdělí pole na jednotlivá pořadí
						elements.add(Integer.parseInt(element)); // a ta přidá do seznam

					map[x][y] = elements.toArray(new Integer[elements.size()]); // převede seznam na pole a uloží
				} else // když zde je zeď
					map[x][y] = new Integer[]{WALL}; // nastav zde zeď
				index++; // posuň se v textu
			}
		}

		// BFS - hledání cesty
		Queue<Pos> path = new LinkedList<>(); // samotná cesta
		Pos frontier = Map.center; // aktuální pozice, na začátku jsme na startu
		index = 0; // aktuální pořadí

		while (frontier != Pos.none) // dokud máme co procházet
		{
			Pos next = Pos.none; // budoucí průchod
			Pos[] adjacent = Pos.adjacent(frontier); // získáme okolí pozice
			for (Pos v : adjacent) // projdeme okolí pozice
			{
				if (Common.aContainsO(index + 1, map[v.x][v.y])) { // pokud pozice obsahuje správné pořadí
					path.add(v); // přidej do cesty
					next = v; // nastav jako aktuální pozici
					break; // vyskoč
				}
			}
			index++; // posuň se v cestě
			frontier = next; // přeřazení, minulá budoucnoust = současnost
		}
		this.path = path; // nastav cestu
	}
	public static Path genPath_BFS(Map map) {
		int request = TileType.LightOn;
		Queue<Pos> retVal = new LinkedList<>();
		Pos last = null;
		boolean finished = false;
		while (!finished) {
			//// BFS
			// INICIALIZACE
			java.util.Map<Pos, Pos> parents = new HashMap<>(); // pole potomek-rodič
			parents.put(map.curPos, Pos.none); // start je kořen

			java.util.Map<Pos, Integer> orients = new HashMap<>(); // pole potomek-rodič
			orients.put(map.curPos, map.curOrient);


			List<Pos> frontier = new ArrayList<>(); // otevřené uzly
			frontier.add(map.curPos); // start je první
			Pos nearest = Pos.none; // pozice nejbližšího bloku

			// PRŮCHOD GRAFEM (MAPOU)
			hledani:
			// označení vnější smyčky
			while (frontier.size() != 0) // dokud máme co procházet
			{
				List<Pos> next = new ArrayList<>(); // budoucí průchod
				for (Pos parent : frontier) // projdeme otevřené uzly
				{
					Pos[] adjacent = Pos.adjacent(parent, orients.get(parent)); // získáme okolí uzlu
					for (Pos child : adjacent) // projdeme okolí uzlu
					{
						if (!parents.containsKey(child) && map.get(child) != TileType.Wall)
						// pokud máme fresh uzel a ten není zábrana
						{
							parents.put(child, parent); // Nastavit rodiče -> navštíven
							if (map.get(child) == request) { // Pokud jsme narazili na požadovaný blok
								nearest = child; // přiřadit
								break hledani; // TADÁ ODSUD
							}
							next.add(child); // Přidat do dalšího průchodu
							orients.put(child, Pos.directionOf(parent, child));
						}

					}
				}
				frontier = next; // přeřazení, minulá budoucnoust = současnost
			} // TADÁ SEM

			// HLEDÁNÍ CESTY
			if (nearest == Pos.none) // Pokud žádný blok nebyl nalezen
				finished = true;


			Stack<Pos> inverse = new Stack<>(); // LIFO, zde bude pořadí cíl->start
			Pos explorPos = nearest; // první bod
			while (explorPos != Pos.none) { // procházet od potomků k rodičům, dokud nejsme na startu
				inverse.push(explorPos); // Přidat uzel
				map.set(explorPos, TileType.LightOff);
				explorPos = parents.get(explorPos); // Posunout se dopředu
			}
			while (inverse.size() != 0) { // postupné převrácení, nové pořadí je start->cíl
				if(inverse.peek()!=last)
					retVal.add(last = inverse.pop());
				else
					inverse.pop();
			}
			map.curPos = nearest;
		}
		return new Path(retVal);
	}
}
