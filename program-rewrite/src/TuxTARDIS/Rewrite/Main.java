package TuxTARDIS.Rewrite;

import TuxTARDIS.Rewrite.Mechanics.HardwareRouter;
import TuxTARDIS.Rewrite.Mechanics.PilotType;
import TuxTARDIS.Rewrite.Mechanics.TuxPilot;
import TuxTARDIS.Rewrite.Navigation.Map;
import TuxTARDIS.Rewrite.Navigation.Path;
import TuxTARDIS.Rewrite.Navigation.TileType;
import TuxTARDIS.Rewrite.Utils.Effects;
import TuxTARDIS.Rewrite.Utils.MapSelector;
import TuxTARDIS.Rewrite.VM.CPU;
import TuxTARDIS.Rewrite.VM.Instruction;
import lejos.hardware.BrickFinder;
import lejos.hardware.Button;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3GyroSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.concurrent.BlockingQueue;

/**
 * Třída hlavního programu
 */
class Main {

	/**
	 * Hlavní funkce programu, entry-point
	 *
	 * @param nic Ignorované argumenty
	 */
	public static void main(String[] nic) {
		Button.LEDPattern(5); // bliká červená
		Const.load();
		Button.LEDPattern(3); // svítí zelená
		Effects.twobeep();
		MapSelector select = new MapSelector("Zadejte cislo:");
		int mn = select.getNumber(1, Const.MAPS, 1); // zeptá se na číslo mapy
		Button.LEDPattern(6); // bliká oranžová
		String mapStr = load_txt(Const.MAP_DIR + "/" + mn + ".map"); // načte mapu
		Map map = new Map(mapStr); // inicializuje mapu
		Path path = new Path(mapStr); // inicializuje cestu
		BlockingQueue<Instruction> exe = CPU.streamize(map, path);

		TuxPilot.PilotProperties properties = new TuxPilot.PilotProperties(
				Const.TRACKWIDTH, Const.DIAMETER,
				new EV3LargeRegulatedMotor(getPort(Const.RIGHT_PORT)),
				new EV3LargeRegulatedMotor(getPort(Const.LEFT_PORT)),
				new EV3TouchSensor(getPort(Const.TOUCH_PORT_FRONT)),
				new EV3TouchSensor(getPort(Const.TOUCH_PORT_BACK)),
				new EV3GyroSensor(getPort(Const.GYRO_PORT)),
				Const.TRAVELBACK);
		HardwareRouter hw = new HardwareRouter(PilotType.TuxovaTARDIS, properties,
				new EV3UltrasonicSensor(getPort(Const.SONIC_PORT)));

		hw.setTravelSpeed(Const.SPEED);
		hw.setRotateSpeed(Const.SPEED);
		CPU cpu = new CPU(hw);
		Button.LEDPattern(1); // svítí zelená
		System.out.println("* ENTER -> RUN *");
		Effects.twobeep();
		Button.ENTER.waitForPress();

		Button.LEDPattern(4); // bliká zelená

		cpu.runStream(exe, map);

		Button.LEDPattern(1); // svítí oranžová
		Effects.play_tune(); // zahraje šílenou melodii :D
		while (Button.ESCAPE.isUp()) {
			Map newMap = new Map(mapStr);
			newMap.curPos = map.curPos;
			newMap.curOrient = map.curOrient;
			newMap.set(map.curPos, TileType.LightOff);
			path = Path.genPath_BFS(new Map(newMap));
			cpu.runStream(CPU.streamize(newMap, path), newMap);
		}
	}

	/**
	 * Přečte texťák
	 *
	 * @param name Jméno souboru
	 * @return Text souboru
	 */
	public static String load_txt(String name) {
		StringBuilder sb = new StringBuilder(512); // buffer souboru
		try { // proti vyjímce
			FileInputStream stream = new FileInputStream(name); // otevře soubor
			Reader r = new InputStreamReader(stream, "UTF-8"); // otevře čteč souboru
			int c; // přečte soubor
			while ((c = r.read()) != -1) {
				sb.append((char) c);
			}
			stream.close(); // zavře soubor
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return sb.toString(); // vrátí buffer
	}
	public static Port getPort(String name){
		return BrickFinder.getLocal().getPort(name);
	}
}